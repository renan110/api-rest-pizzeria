package dto;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Pizza {
    private int pno;
    private String name;
    private String base;
    private float basePrice;
    List<Ingredient> ingredients;

    public Pizza() {
    }

    public Pizza(int pno,String name, String base, float price, List<Ingredient> ingredients) {
        this.pno = pno;
        this.name = name;
        this.base = base;
        this.basePrice = price;
        this.ingredients = ingredients;
    }

    public int getPno(){
        return this.pno;
    }

    public String getName() {
        return this.name;
    }

    public String getBase() {
        return this.base;
    }
    
    public float getBasePrice() {
        return this.basePrice;
    }

    public List<Ingredient> getIngredients() {
        return this.ingredients;
    }
    public void setPno(int pno) {
        this.pno = pno;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setBase(String base) {
        this.base = base;
    }

    public void setBasePrice(float basePrice) {
        this.basePrice = basePrice;
    }
    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Pizza)) {
            return false;
        }
        Pizza pizza = (Pizza) o;
        return Objects.equals(name, pizza.name) && Objects.equals(base, pizza.base) && Objects.equals(basePrice, pizza.basePrice) && Objects.equals(ingredients, pizza.ingredients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, base, basePrice, ingredients);
    }

    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            ", base='" + getBase() + "'" +
            ", price='" + getBasePrice() + "'" +
            ", ingredients='" + getIngredients() + "'" +
            "}";
    }


}
