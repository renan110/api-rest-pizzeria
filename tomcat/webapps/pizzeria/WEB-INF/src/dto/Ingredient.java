package dto;

import java.util.Objects;


public class Ingredient {
    private int ino;
    private String name;
    private float price;


    public Ingredient() {
    }

    public Ingredient(int ino, String name, float price) {
        this.ino = ino;
        this.name = name;
        this.price = price;
    }

    public int getIno() {
        return this.ino;
    }


    public String getName() {
        return this.name;
    }


    public float getPrice() {
        return this.price;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Ingredient)) {
            return false;
        }
        Ingredient ingredient = (Ingredient) o;
        return ino == ingredient.ino && Objects.equals(name, ingredient.name) && price == ingredient.price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ino, name, price);
    }
  
    @Override
    public String toString() {
        return "{" +
            " id='" + getIno() + "'" +
            ", name='" + getName() + "'" +
            ", price='" + getPrice() + "'" +
            "}";
    }
    

}
