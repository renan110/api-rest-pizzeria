package dto;

import java.util.List;
import java.util.Objects;
import java.sql.Date;
import java.time.LocalDate;



public class Commande {
    private int cno;
    private int uno;
    private Date commandeDate;
    List<Pizza> pizzas;

    public Commande(){
        this.commandeDate = Date.valueOf(LocalDate.now());
    }

    public Commande(int cno,int uno,List<Pizza> pizzas){
        this.cno = cno;
        this.uno = uno;
        this.commandeDate = Date.valueOf(LocalDate.now());
        this.pizzas = pizzas;
    }

    public int getCno(){
        return this.cno;
    }

    public void setCno(int cno){
        this.cno = cno;
    }

    public int getUno(){
        return this.uno;
    }

    public void setUno(int uno){
        this.uno = uno;
    }

    public Date getCommandeDate(){
        return this.commandeDate;
    }

    public void setCommandeDate(Date date){
        this.commandeDate = date;
    }

    public List<Pizza> getPizzas() {
        return this.pizzas;
    }

    public void setPizzas(List<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Commande)) {
            return false;
        }
        Commande commande = (Commande) o;
        return Objects.equals(uno, commande.uno) && Objects.equals(commandeDate, commande.commandeDate) && Objects.equals(pizzas, commande.pizzas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uno, commandeDate, pizzas);
    }

    @Override
    public String toString() {
        return "{" +
            " uno='" + getUno() + "'" +
            ", commandeDate='" + getCommandeDate() + "'" +
            ", pizzas='" + getPizzas() + "'" +
            "}";
    }

}