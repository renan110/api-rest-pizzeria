package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dto.User;

public class UserDAO {
    private DS ds = new DS();
    private boolean isIUTDatabase = false;

    public User find(String login) throws SQLException {
        try(Connection connection = ds.getConnection(isIUTDatabase)){
            String query = "SELECT * FROM users WHERE login = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                int uno = rs.getInt("uno");
                String pwd = rs.getString("pwd");
                return new User(uno, login, pwd);
            }
        }
        return null;
    }

}
