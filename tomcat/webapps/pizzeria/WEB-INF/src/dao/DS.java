package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DS {

    public DS() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (Exception e) {
            System.out.println("" + e);
        }
    }

    public Connection getConnection(boolean isIUTDatabase) {
        if (isIUTDatabase) {
            try {
                return DriverManager.getConnection("jdbc:postgresql://psqlserv:5432/but2", "renandeclercqetu", "moi");
                //return DriverManager.getConnection("jdbc:postgresql://psqlserv:5432/but2", "nathanaccartetu", "moi");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            try {
                return DriverManager.getConnection("jdbc:postgresql://localhost:5432/pizzeria", "renan", "moi");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

}