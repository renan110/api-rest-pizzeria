package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import dto.Commande;
import dto.Pizza;

public class CommandeDAO {
    private DS ds = new DS();
    private boolean isIUTDatabase = false;

    public List<Commande> findAll() {
        List<Commande> res = new ArrayList<>();
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "SELECT * FROM commandes;";
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int cno = rs.getInt("cno");
                res.add(findById(cno));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    public Commande findById(int cno) {
        Commande commande = new Commande();
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "SELECT * FROM commandes WHERE cno = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, cno);
            ResultSet rs = ps.executeQuery();
            int uno = 0;
            Date date;
            while (rs.next()) {
                uno = rs.getInt("uno");
                date = rs.getDate("commandeDate");
                commande.setCno(cno);
                commande.setUno(uno);
                commande.setCommandeDate(date);
                System.out.println(date);
            }

            query = "SELECT * FROM pizzasCommandes WHERE cno = ?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, cno);
            rs = ps.executeQuery();
            List<Integer> pnoList = new ArrayList<>();
            while (rs.next()) {
                pnoList.add(rs.getInt("pno"));
            }

            query = "SELECT * FROM pizzas WHERE pno = ?";
            ps = connection.prepareStatement(query);
            Pizza pizza = null;
            PizzaDAO pizzaDAO = new PizzaDAO();
            List<Pizza> pizzas = new ArrayList<>();
            for (int pno : pnoList) {
                ps.setInt(1, pno);
                rs = ps.executeQuery();
                while (rs.next()) {
                    pizza = pizzaDAO.findById(pno);

                    pizzas.add(pizza);
                }
                commande.setPizzas(pizzas);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return commande;
    }

    public void insertCommande(Commande commande) throws SQLException {
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "INSERT INTO commandes(cno, uno, commandeDate) VALUES (?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, commande.getCno());
            ps.setInt(2, commande.getUno());
            ps.setDate(3, commande.getCommandeDate());
            ps.executeUpdate();

            query = "INSERT INTO pizzasCommandes(cno, pno) VALUES (?, ?)";
            PreparedStatement ps2 = connection.prepareStatement(query);
            for (Pizza pizza : commande.getPizzas()) {
                ps2.setInt(1, commande.getCno());
                ps2.setInt(2, pizza.getPno());
                ps2.addBatch();
            }
            ps2.executeBatch();
        }
    }

    public void deleteCommande(int cno) throws SQLException {
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "DELETE FROM pizzasCommandes WHERE cno = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, cno);
            ps.executeUpdate();

            query = "DELETE FROM commandes WHERE cno = ?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, cno);
            ps.executeUpdate();
        }
    }

    public float getFinalPrice(int cno) {
        float finalPrice = 0;
        PizzaDAO pizzaDAO = new PizzaDAO();
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "SELECT pno FROM pizzasCommandes WHERE cno = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, cno);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                int pno = rs.getInt("pno");
                finalPrice += pizzaDAO.getFinalPrice(pno);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (float) Math.round((finalPrice) * 100) / 100;
    }
}