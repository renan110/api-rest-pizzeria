package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dto.Ingredient;
import dto.Pizza;

public class PizzaDAO {
    private DS ds = new DS();
    private boolean isIUTDatabase = false;

    public Pizza findById(int pno) {
        Pizza pizza = new Pizza();
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "SELECT * FROM pizzas WHERE pno = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, pno);
            ResultSet rs = ps.executeQuery();
            String name = "";
            String base = "";
            float basePrice = 0;
            while (rs.next()) {
                name = rs.getString("name");
                base = rs.getString("base");
                basePrice = rs.getFloat("basePrice");
                pizza.setPno(pno);
                pizza.setName(name);
                pizza.setBase(base);
                pizza.setBasePrice(basePrice);
            }

            query = "SELECT * FROM pizzaIngredients WHERE pno = ?";
            ps = connection.prepareStatement(query);
            ps.setInt(1, pno);
            rs = ps.executeQuery();
            List<Ingredient> ingredients = new ArrayList<>();
            List<Integer> inoList = new ArrayList<>();
            while (rs.next()) {
                inoList.add(rs.getInt("ino"));
            }

            query = "SELECT * FROM ingredients WHERE ino = ?";
            ps = connection.prepareStatement(query);
            Ingredient ingredient = null;
            for (int ino : inoList) {
                ps.setInt(1, ino);
                rs = ps.executeQuery();
                String ingredientName = "";
                float ingredientPrice = 0;
                while (rs.next()) {
                    ingredientName = rs.getString("name");
                    ingredientPrice = rs.getFloat("price");
                    ingredient = new Ingredient(ino, ingredientName, ingredientPrice);
                    ingredients.add(ingredient);
                }
                pizza.setIngredients(ingredients);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return pizza;
    }

    public List<Pizza> findAll() {
        List<Pizza> res = new ArrayList<>();
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "SELECT * FROM pizzas;";
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            List<Integer> pnoList = new ArrayList<>();
            while (rs.next()) {
                int pno = rs.getInt("pno");
                pnoList.add(pno);
            }
            for (Integer pno : pnoList) {
                res.add(findById(pno));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    public void insertPizza(Pizza pizza) throws SQLException{
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "INSERT INTO pizzas(pno, name, base, basePrice) VALUES (?, ?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, pizza.getPno());
            ps.setString(2, pizza.getName());
            ps.setString(3, pizza.getBase());
            ps.setFloat(4, pizza.getBasePrice());
            ps.executeUpdate();

            query = "INSERT INTO pizzaIngredients(ino, pno) VALUES (?, ?)";
            PreparedStatement ps2 = connection.prepareStatement(query);
            for (Ingredient ingredient : pizza.getIngredients()) {
                ps2.setInt(1, ingredient.getIno());
                ps2.setInt(2, pizza.getPno());
                ps2.addBatch();
            }
            ps2.executeBatch();
        }
    }

    public int delete(int pno) throws SQLException {
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "DELETE FROM pizzas WHERE pno = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, pno);
            return ps.executeUpdate();
        }
    }

    public boolean updatePizzaPrice(int pno, float newPrice) throws SQLException{
        try(Connection connection = ds.getConnection(isIUTDatabase)){
            String query = "UPDATE pizzas SET basePrice = ? WHERE pno = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setFloat(1, newPrice);
            ps.setInt(2, pno);
            int rowsUpdated = ps.executeUpdate();
            return rowsUpdated > 0;
        }
    }

    public boolean addIngredient(int ino, int pno) throws SQLException{
        try(Connection connection = ds.getConnection(isIUTDatabase)){
            String query = "INSERT INTO pizzaIngredients(ino,pno) VALUES (?,?)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, ino);
            ps.setInt(2, pno);
            int rowsUpdated = ps.executeUpdate();
            return rowsUpdated > 0;
        }
    }

    public boolean deleteIngredient(int ino , int pno) throws SQLException{
        try(Connection connection = ds.getConnection(isIUTDatabase)){
            String query = "DELETE FROM pizzaIngredients WHERE ino = ? AND pno = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, ino);
            ps.setInt(2, pno);
            int rowsUpdated = ps.executeUpdate();
            return rowsUpdated > 0;
        }
    }

    public float getFinalPrice(int pno) throws SQLException {
        Pizza pizza = findById(pno);
        float finalPrice = pizza.getBasePrice();
        try(Connection connection = ds.getConnection(isIUTDatabase)){
            String query = "SELECT * FROM pizzaIngredients WHERE pno = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, pno);
            ResultSet rs = ps.executeQuery();
            List<Integer> inoList = new ArrayList<>();
            while(rs.next()){
                int ino = rs.getInt("ino");
                inoList.add(ino);
            }

            IngredientDAO ingredientDAO = new IngredientDAO();
            for(Integer ino : inoList){
                Ingredient ingredient = ingredientDAO.findById(ino);
                finalPrice += ingredient.getPrice();
            }
        }

        return finalPrice;
    }

    public boolean updatePizzaName(int pno, String newName) throws SQLException {
        try(Connection connection = ds.getConnection(isIUTDatabase)){
            String query = "UPDATE pizzas SET name = ? WHERE pno = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, newName);
            ps.setInt(2, pno);
            int rowsUpdated = ps.executeUpdate();
            return rowsUpdated > 0;
        }
    }
}
