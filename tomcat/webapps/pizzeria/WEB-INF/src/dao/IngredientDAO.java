package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.Ingredient;

public class IngredientDAO {
    private DS ds = new DS();
    private boolean isIUTDatabase = false;

    public Ingredient findById(int ino) {
        Ingredient ingredient = null;
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "SELECT * FROM ingredients WHERE ino = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, ino);
            ResultSet rs = ps.executeQuery();
            String name = "";
            float price = 0;
            while (rs.next()) {
                name = rs.getString("name");
                price = rs.getFloat("price");
            }
            ingredient = new Ingredient(ino, name, price);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        } 
        return ingredient;
    }

    public String getNameByID(int ino){
        Ingredient ingredient = findById(ino);
        return ingredient.getName();
    }

    public List<Ingredient> findAll() throws SQLException {
        List<Ingredient> res = new ArrayList<>();
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "SELECT * FROM ingredients;";
            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int ino = rs.getInt("ino");
                String name = rs.getString("name");
                float price = rs.getFloat("price");
                Ingredient ingredient = new Ingredient(ino, name,price);
                res.add(ingredient);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public boolean insertIngredient(Ingredient ingredient) throws SQLException {
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "INSERT INTO ingredients(ino, name, price) VALUES (?, ?, ?)";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, ingredient.getIno());
            ps.setString(2, ingredient.getName());
            ps.setFloat(3, ingredient.getPrice());
            return ps.executeUpdate() > 0;
        }
    }

    public int delete(int ino) throws SQLException {
        try (Connection connection = ds.getConnection(isIUTDatabase)) {
            String query = "DELETE FROM ingredients WHERE ino = ?";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, ino);
            return ps.executeUpdate();
        }
    }
}
