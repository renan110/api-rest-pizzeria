package controleurs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.IngredientDAO;
import dto.Ingredient;

@WebServlet("/ingredients/*")
public class IngredientRestAPI extends HttpServlet {
    IngredientDAO ingredientDAO = new IngredientDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        res.setContentType("application/json;charset=UTF-8");
        PrintWriter out = res.getWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        String info = req.getPathInfo();
        // GET /ingredients : return all ingredients
        if (info == null || info.equals("/")) {
            String jsonstring;
            try {
                jsonstring = objectMapper.writeValueAsString(ingredientDAO.findAll());
                out.print(jsonstring);

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return;
        }

        String[] splits = info.split("/");
        int id = Integer.parseInt(splits[1]);
        if (splits.length == 2 || splits.length == 3) {
            if (ingredientDAO.findById(id).getName().equals("")) {
                res.sendError(HttpServletResponse.SC_NOT_FOUND);
                out.println("Ingredient not found");
                return;
            } else {
                if (splits.length == 2) {
                    // GET /ingredients/{id} : return one ingredient with {id}
                    out.print(objectMapper.writeValueAsString(ingredientDAO.findById(id)));

                } else if (splits.length == 3 && splits[2].equals("name")) {
                    // GET /ingredients/{id}/name : return one ingredient name with {id}/name
                    out.print(objectMapper.writeValueAsString(ingredientDAO.getNameByID(id)));

                } else {
                    res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
            }
        } else {
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {

        StringBuilder data = new StringBuilder();
        BufferedReader reader = req.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            data.append(line);
        }

        PrintWriter out = res.getWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        Ingredient addedIngredient = objectMapper.readValue(data.toString(), Ingredient.class);
        try {
            // POST /ingredients : add an ingredient in the table
            boolean success = ingredientDAO.insertIngredient(addedIngredient);
            if (success) {
                out.println("You added : " + ingredientDAO.getNameByID(addedIngredient.getIno()));

            } else {
                res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {

        res.setContentType("application/json;charset=UTF-8");
        PrintWriter out = res.getWriter();
        String pathInfo = req.getPathInfo();
        String[] pathParts = pathInfo.split("/");
        if (pathParts.length != 2) {
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        int ino = Integer.parseInt(pathParts[1]);
        try {
            // DELETE /ingredients/{id} : delete a ingredient
            String Ingredientname = ingredientDAO.getNameByID(ino);
            int rowsDeleted = ingredientDAO.delete(ino);
            if (rowsDeleted == 0) {
                res.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            out.println("You have removed : " + Ingredientname);
        } catch (SQLException e) {
            e.printStackTrace();
            res.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

}