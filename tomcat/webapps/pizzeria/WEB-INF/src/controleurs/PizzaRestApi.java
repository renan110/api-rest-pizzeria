package controleurs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.IngredientDAO;
import dao.PizzaDAO;
import dto.Pizza;

@WebServlet("/pizzas/*")
public class PizzaRestApi extends HttpServlet {
    private PizzaDAO pizzaDAO = new PizzaDAO();
    private IngredientDAO ingredientDAO = new IngredientDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        res.setContentType("application/json;charset=UTF-8");
        PrintWriter out = res.getWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        String pathInfo = req.getPathInfo();
        // GET /pizzas : return all pizzas
        if (pathInfo == null || pathInfo.equals("/")) {
            String jsonstring = objectMapper.writeValueAsString(pizzaDAO.findAll());
            out.print(jsonstring);

            return;
        }

        String[] pathParts = pathInfo.split("/");
        int pno = Integer.parseInt(pathParts[1]);
        if (pathParts.length == 2 || pathParts.length == 3) {
            if (pizzaDAO.findById(pno).getPno() == 0) {
                res.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            } else {
                if (pathParts.length == 2) {
                    // GET /pizzas/{id} : return a pizza with {id}
                    out.print(objectMapper.writeValueAsString(pizzaDAO.findById(pno)));

                } else if (pathParts.length == 3 && pathParts[2].equals("prixfinal")) {
                    // GET /pizzas/{id}/prixfinal : return final price of a pizza
                    try {
                        // out.print(objectMapper.writeValueAsString("The pizza " + pizzaDAO.findById(pno).getName()
                        //         + " costs " + pizzaDAO.getFinalPrice(pno) + " €"));
                        out.println(pizzaDAO.getFinalPrice(pno));

                    } catch (SQLException e) {
                        e.printStackTrace();
                        res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    }
                } else {
                    res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
            }
        } else {
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {

        res.setContentType("application/json;charset=UTF-8");
        PrintWriter out = res.getWriter();
        StringBuilder data = new StringBuilder();
        BufferedReader reader = req.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            data.append(line);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        Pizza addedPizza;
        String info = req.getPathInfo();
        String[] pathParts = info.split("/");

        if (info == null || info.equals("/")) {
            addedPizza = objectMapper.readValue(data.toString(), Pizza.class);
            // POST /pizzas : Insert pizza with his ingredients
            try {
                pizzaDAO.insertPizza(addedPizza);
            } catch (SQLException e) {
                e.printStackTrace();
                if(pizzaDAO.findById(addedPizza.getPno()).getIngredients() != null){
                    res.sendError(HttpServletResponse.SC_CONFLICT, e.getMessage());
                }else{
                        try {
                            pizzaDAO.delete(addedPizza.getPno());
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                            res.sendError(HttpServletResponse.SC_NOT_FOUND, "SQL Exception\n" + e.getMessage());
                        }
                    res.sendError(HttpServletResponse.SC_NOT_FOUND, "SQL Exception\n" + e.getMessage());
                }
            }
            out.println(objectMapper.writeValueAsString(pizzaDAO.findById(addedPizza.getPno())));

        } else if (pathParts.length == 2) {
            int pno = Integer.parseInt(pathParts[1]);
            if (pizzaDAO.findById(pno).getPno() == 0) {
                res.sendError(HttpServletResponse.SC_NOT_FOUND, "Pizza " + pno + " does not exist");
                return;
            } else {
                // POST /pizzas/{id} : add one ingredient in a pizza
                int ino = Integer.parseInt(data.toString());
                try {
                    pizzaDAO.addIngredient(ino, pno);
                    out.print(objectMapper.writeValueAsString(ingredientDAO.findById(ino)));

                } catch (SQLException e) {
                    e.printStackTrace();
                    try {
                        if (!ingredientDAO.findAll().contains(ingredientDAO.findById(ino))) {
                            res.sendError(HttpServletResponse.SC_NOT_FOUND, "Ingredient " + ino + " does not exist");
                        } else {
                            res.sendError(HttpServletResponse.SC_CONFLICT, "Ingredient " + ino
                                    + " is already in the pizza " + pno + " : " + pizzaDAO.findById(pno).getName());
                        }
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        } else {
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {

        res.setContentType("application/json;charset=UTF-8");
        PrintWriter out = res.getWriter();
        StringBuilder data = new StringBuilder();
        BufferedReader reader = req.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            data.append(line);
        }
        String pathInfo = req.getPathInfo();
        String[] pathParts = pathInfo.split("/");

        if (pathParts.length != 2 && pathParts.length != 3) {
            System.out.println(pathParts.length);
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        int pno = Integer.parseInt(pathParts[1]);
        if (pizzaDAO.findById(pno) == null) {
            res.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        if (pathParts.length == 2) {
            // DELETE /pizzas/{pno} : delete a pizza
            try {
                String pizzaName = pizzaDAO.findById(pno).getName();
                int rowsDeleted = pizzaDAO.delete(pno);
                if (rowsDeleted == 0) {
                    res.sendError(HttpServletResponse.SC_NOT_FOUND,
                            "You can't remove pizza n°" + pno + " because it does not exist");
                    return;
                } else {
                    out.println("You have removed the pizza " + pizzaName + "(" + pno + ")");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else if (pathParts.length == 3) {
            int ino = Integer.parseInt(pathParts[2]);
            try {
                // DELETE /pizza/{pno}/{ino} : delete an ingredient of a pizza
                boolean success = pizzaDAO.deleteIngredient(ino, pno);
                if (success) {
                    out.println(
                            "You have removed the ingredient " + ingredientDAO.findById(ino).getName() + "(" + ino
                                    + ") from pizza " + pizzaDAO.findById(pno).getName() + "(" + pno + ")");

                } else {
                    if(ingredientDAO.findById(ino).getName().equals("")){
                        res.sendError(HttpServletResponse.SC_NOT_FOUND, "Ingredient " + ino + " does not exist");
                    }else{
                        res.sendError(HttpServletResponse.SC_NOT_FOUND, pizzaDAO.findById(pno).getName() + " (" + pno + ") does not contain ingredient " + ino);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        }

    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (req.getMethod().equalsIgnoreCase("PATCH")) {
            doPatch(req, res);
        } else {
            super.service(req, res);
        }
    }

    public void doPatch(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        res.setContentType("application/json;charset=UTF-8");
        PrintWriter out = res.getWriter();

        // Récupération de l'id de la pizza à modifier depuis l'URL
        String pathInfo = req.getPathInfo();
        String[] pathParts = pathInfo.split("/");

        if ((pathParts.length != 3)) {
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        int pno = Integer.parseInt(pathParts[1]);

        // Récupération du nouveau prix depuis le corps de la requête
        BufferedReader reader = req.getReader();
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }

        boolean success = false;

        if (pathParts[2].equalsIgnoreCase("prixBase")) {
            float newBasePrice = Float.parseFloat(stringBuilder.toString());
            // Modification du prix de la pizza en utilisant la méthode updatePizzaPrice
            try {
                success = pizzaDAO.updatePizzaPrice(pno, newBasePrice);
                // Envoi de la réponse au client
                if (success) {

                    Pizza pizza = pizzaDAO.findById(pno);
                    out.println(pizza.getName() + "(" + pno + ") new base price   : " + pizza.getBasePrice()
                            + "€.\nFinal price is " + (float) Math.round((pizzaDAO.getFinalPrice(pno)) * 100) / 100
                            + "€");
                } else {
                    res.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (pathParts[2].equalsIgnoreCase("name")) {
            String newName = stringBuilder.toString();

            try {
                success = pizzaDAO.updatePizzaName(pno, newName);
                if (success) {

                    Pizza pizza = pizzaDAO.findById(pno);

                    out.println("Pizza " + pno + " new name : " + pizza.getName());
                } else {
                    res.sendError(HttpServletResponse.SC_NOT_FOUND, "Pizza " + pno + " not found");

                }
            } catch (SQLException e) {
                e.printStackTrace();
                res.sendError(HttpServletResponse.SC_NOT_FOUND, "Pizza " + pno + " not found");

            }
        } else {
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

}
