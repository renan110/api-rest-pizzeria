package controleurs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import dao.CommandeDAO;
import dto.Commande;

@WebServlet("/commandes/*")
public class CommandeRestApi extends HttpServlet {
    CommandeDAO commandeDAO = new CommandeDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        res.setContentType("application/json;charset=UTF-8");
        PrintWriter out = res.getWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS , false);
        String info = req.getPathInfo();
        // GET /commandes : return all commandes
        if (info == null || info.equals("/")) {
            String jsonstring;
            jsonstring = objectMapper.writeValueAsString(commandeDAO.findAll());
            out.print(jsonstring);
            return;
        }

        String[] splits = info.split("/");
        int cno = Integer.parseInt(splits[1]);

        if (splits.length == 2 || splits.length == 3) {
            if (commandeDAO.findById(cno).getCno() == 0) {
                res.sendError(HttpServletResponse.SC_NOT_FOUND, "Order " + cno +" does not exist");
                return;
            } else {
                if (splits.length == 2) {
                    // GET /commandes/{id} : return one commande with {id}
                    out.print(objectMapper.writeValueAsString(commandeDAO.findById(cno)));
                }else if(splits.length == 3 && splits[2].equals("prixfinal")){
                    // GET /commandes/{id}/prixfinal : return final price of a commande
                    // out.print(objectMapper.writeValueAsString("Order n° "+ cno + " final price : " + commandeDAO.getFinalPrice(cno) + "€"));
                    out.println(commandeDAO.getFinalPrice(cno));
                }else{
                    res.sendError(HttpServletResponse.SC_BAD_REQUEST);
                }
            }
        } else {
            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {

        res.setContentType("application/json;charset=UTF-8");
        StringBuilder data = new StringBuilder();
        BufferedReader reader = req.getReader();
        String line;
        while ((line = reader.readLine()) != null) {
            data.append(line);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS , false);
        PrintWriter out = res.getWriter();

        Commande addedCommande = new Commande();
        // POST /commandes : add a commande in the table
        try {
            addedCommande = objectMapper.readValue(data.toString(), Commande.class);
            commandeDAO.insertCommande(addedCommande);
            out.println(objectMapper.writeValueAsString(commandeDAO.findById(addedCommande.getCno())));
        } catch (SQLException | JsonProcessingException e) {
            int cno = addedCommande.getCno();
            if(commandeDAO.findById(cno).getPizzas() == null){
                try {
                    commandeDAO.deleteCommande(cno);
                    res.sendError(HttpServletResponse.SC_NOT_FOUND, "Order n°" + cno + " was not added due to an error in the parameters\n"+ e.getLocalizedMessage());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
            res.sendError(HttpServletResponse.SC_CONFLICT, "Order n°" + cno + " already exists\n"+ e.getLocalizedMessage());
        }

    }
}