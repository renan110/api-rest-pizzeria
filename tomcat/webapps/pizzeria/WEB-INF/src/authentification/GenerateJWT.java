package authentification;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDAO;
import dto.User;

@WebServlet("/users/token")
public class GenerateJWT extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDAO userDAO = new UserDAO();

        String login = req.getParameter("login");
        String pwd = req.getParameter("pwd");
        PrintWriter out = resp.getWriter();

        User user = new User();
        try {
            user = userDAO.find(login);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(user != null && user.hasPassword(pwd)){
            String token = JwtManager.createJWT();
            req.setAttribute("token", token);
            out.println(token);
        }else{
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Wrong login or pwd");
        }
    }
}
