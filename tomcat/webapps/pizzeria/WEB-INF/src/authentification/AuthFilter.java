package authentification;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.jsonwebtoken.io.IOException;

public class AuthFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        if ("GET".equalsIgnoreCase(httpRequest.getMethod())) {
            try {
                chain.doFilter(request, response);
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
            return;
        }

        if (isAuthorized(httpRequest, httpResponse)) {
            try {
                // Token is valid, add claims to request for future use
                chain.doFilter(request, response);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            // No token found, send error response
            handleBadToken(httpRequest, httpResponse);
            return;
        }
    }

    public void handleBadToken(HttpServletRequest req, HttpServletResponse resp) {
        try {
            req.getRequestDispatcher("/Unauthorized").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isAuthorized(HttpServletRequest req, HttpServletResponse resp){
        HttpServletRequest httpRequest = (HttpServletRequest) req;
        String token = httpRequest.getHeader("Authorization");
        if (token != null && token.startsWith("Bearer ")) {
            token = token.substring(7); // Remove "Bearer " prefix
            try {
                JwtManager.decodeJWT(token);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

}
