# Projet API REST Pizzeria BUT informatique 2eme année

    Groupe H
    DECLERCQ Renan
    ACCART Nathan

Ce repertoire git contient uniquemment le contexte web du projet API REST Pizzeria. 

Les rapports sous différents formats sont dans le dossier ``rapport``.

Les tables de la base de données pizzeria sont dans le dossier ``rapport`` > ``tables_SQL``

La collection postman au format JSON de nos requêtes testées sont dans le dossier ``requete_collection_postman``

Conseil : le rapport au format HTML est plus agréable à lire que celui au format PDF.

Plateforme de développement d'API utilisée :
 - Postman
 - RESTED
  
Serveur web utilisé : 
 - Tomcat

Librairies utilisées :
 - driverPSQL
 - jackson
 - JJWT
 - servlet-api

SGBDR utilisé :
 - postgreSQL

IDE utilisées :
 - VSCode / VSCodium
 - IntelliJ IDEA