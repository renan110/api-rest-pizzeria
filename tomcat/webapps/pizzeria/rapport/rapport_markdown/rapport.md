# Requête cURL - SAE API REST Pizzeria

!!! info Groupe
    Renan DECLERCQ
    Nathan ACCART
    Groupe H

!!! info Repo Gitlab
    https://gitlab.univ-lille.fr/renan.declercq.etu/sae-a021-api-rest-pizzeria.git

## 1. MCD de la base de données pizzeria

![Diagramme de classes projet pizzeria](../images/MCD_pizzeria.svg)

## 2. Script SQL pizzeria.sql

```sql
    -- Suppression des tables 

DROP TABLE IF EXISTS pizzasCommandes;
DROP TABLE IF EXISTS commandes;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS pizzaIngredients;
DROP TABLE IF EXISTS pizzas;
DROP TABLE IF EXISTS ingredients;

-- Creation des tables

CREATE TABLE ingredients(
    ino INT,
    name TEXT,
    price FLOAT,
    CONSTRAINT pk_ingredients PRIMARY KEY (ino)
);

CREATE TABLE pizzas(
    pno INT,
    name TEXT,
    base TEXT,
    basePrice FLOAT,
    CONSTRAINT pk_pizzas PRIMARY KEY (pno)
);

CREATE TABLE pizzaIngredients(
    ino INT,
    pno INT,
    CONSTRAINT pk_pizzaIngredients PRIMARY KEY(ino,pno),
    CONSTRAINT fk_ingredients FOREIGN KEY (ino) 
        REFERENCES ingredients(ino)
        ON DELETE CASCADE,
    CONSTRAINT fk_pizzas FOREIGN KEY (pno) 
        REFERENCES pizzas(pno)
        ON DELETE CASCADE
);

CREATE TABLE users(
    uno INT,
    login TEXT,
    pwd TEXT,
    CONSTRAINT pk_users PRIMARY KEY(uno)
);

CREATE TABLE commandes(
    cno INT,
    uno INT,
    commandeDate DATE,
    CONSTRAINT pk_commandes PRIMARY KEY(cno),
    CONSTRAINT fk_users FOREIGN KEY (uno) 
        REFERENCES users(uno)
        ON DELETE CASCADE
);

CREATE TABLE pizzasCommandes(
    cno INT,
    pno INT,
    CONSTRAINT fk_commandes FOREIGN KEY (cno) REFERENCES commandes(cno),
    CONSTRAINT fk_pizzas FOREIGN KEY (pno) 
        REFERENCES pizzas(pno)
        ON DELETE CASCADE
);


-- Ajouts dans les tables

INSERT INTO ingredients VALUES (1,'pomme de terre',0.80);
INSERT INTO ingredients VALUES (2,'poivrons', 0.50);
INSERT INTO ingredients VALUES (3,'chorizo', 1.0);
INSERT INTO ingredients VALUES (4,'lardons', 1.0);
INSERT INTO ingredients VALUES (5,'aubergines', 0.5);
INSERT INTO ingredients VALUES (6,'champignons',0.80);
INSERT INTO ingredients VALUES (7,'ananas', 0.80);
INSERT INTO ingredients VALUES (8,'fromage', 0.50);
INSERT INTO ingredients VALUES (9,'tomates', 0.50);
INSERT INTO ingredients VALUES (10,'olives', 0.50);
INSERT INTO ingredients VALUES (11,'oeuf', 0.80);

INSERT INTO pizzas VALUES (100,'classico','classique',10.0);
INSERT INTO pizzas VALUES (101,'veggie','fine',10.0);

INSERT INTO pizzaIngredients(ino,pno) VALUES (1,100);
INSERT INTO pizzaIngredients(ino,pno) VALUES (6,100);
INSERT INTO pizzaIngredients(ino,pno) VALUES (8,100);
INSERT INTO pizzaIngredients(ino,pno) VALUES (10,100);
INSERT INTO pizzaIngredients(ino,pno) VALUES (1,101);
INSERT INTO pizzaIngredients(ino,pno) VALUES (2,101);
INSERT INTO pizzaIngredients(ino,pno) VALUES (5,101);
INSERT INTO pizzaIngredients(ino,pno) VALUES (6,101);
INSERT INTO pizzaIngredients(ino,pno) VALUES (8,101);

INSERT INTO users(uno,login,pwd) VALUES (1,'renan',encode(sha256('renan'),'hex'));
INSERT INTO users(uno,login,pwd) VALUES (2,'nathan',encode(sha256('nathan'),'hex'));

INSERT INTO commandes(cno,uno,commandeDate) values (1,1,'2023-10-10');
INSERT INTO commandes(cno,uno,commandeDate) values (2,2,'2023-10-11');

INSERT INTO pizzasCommandes(cno,pno) values (1,100);
INSERT INTO pizzasCommandes(cno,pno) values (1,101);
INSERT INTO pizzasCommandes(cno,pno) values (1,101);
INSERT INTO pizzasCommandes(cno,pno) values (2,100);


-- Requêtes SELECT

SELECT * FROM ingredients;

SELECT * FROM pizzas;

SELECT * FROM pizzaingredients;

SELECT * FROM commandes;

SELECT * FROM pizzasCommandes;

SELECT * FROM users;

```

## 3. Diagramme de classes

![Diagramme de classes projet pizzeria](../images/UML2.svg)

## 4. Requêtes testées 

[Lien vers nos requêtes cURL avec commentaires](../requete_collection_postman/requete_curl.md)

[Lien vers l'exportation de nos requêtes Postman au format JSON](../requete_collection_postman/Pizzeria.postman_collection.json)

