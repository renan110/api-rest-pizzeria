# Requête c URL - SAE API REST Pizzeria

!!! info Groupe
    Renan DECLERCQ
    Nathan ACCART
    Groupe H

!!! info Repo Gitlab
    https://gitlab.univ-lille.fr/renan.declercq.etu/sae-a021-api-rest-pizzeria.git

## 1 - Endpoints token : /users/token

### 1.1 - Requête n° 1 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/users/token?login=renan&pwd=renan"
```

    Description : renvoie le token JWT 

### 1.2 - Requête n° 2 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/users/token?login=renan&pwd=mauvaisMDP"
```

    Description : renvoie une erreur 401 - Non autorisé car le mot de passe est mauvais

### 1.3 - Requête n° 3 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/users/token"
```

    Description : renvoie une erreur 401 - Non autorisé car il n'y a pas les paramètres login et pwd 

## 2 - Endpoints ingredients : /ingredients/*

### 2.1 - Requête n° 1 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/ingredients/"
```

    Description : renvoie la liste de tous les ingrédients au format JSON

### 2.2 - Requête n° 2 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/ingredients/1"
```

    Description : renvoie l'ingrédient n°1 au format JSON

### 2.3 - Requête n° 3 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/ingredients/1/name"
```

    Description : renvoie le nom de l'ingrédient n°1 

### 2.4 - Requête n° 4 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/ingredients/1/abc"
```

    Description : renvoie une R

### 2.5 - Requête n° 5 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/ingredients" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "{
            \"ino\": 101,
            \"name\": \"pepperoni\",
            \"price\": 0.8
    }"
```

    Description : renvoie la chaine de caractère : "You added : pepperoni". Et ajoute l'ingrédient à la base de données

### 2.5 - Requête n° 6 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/ingredients" \
    --header "Content-Type: text/plain" \
    --data "{
            \"ino\": 100,
            \"name\": \"salade\",
            \"price\": 0.5
    }"
```

    Description : renvoie une erreur 401 - Non autorisé car il manque le token d'authentification

### 2.7 - Requête n° 7 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/ingredients/101" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsIm4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --data ""
```

    Description : renvoie une erreur 401 - Non autorisé car le token est faux

### 2.8 - Requête n° 8 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/ingredients/101" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --data ""
```

    Description : renvoie la chaine de caractère : "You have removed : pepperoni". Et supprime l'ingrédient de la base de données

### 2.9 - Requête n° 9 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/ingredients/150/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" 
```

    Description : renvoie une erreur 404 - Non trouvé car l'ingrédient 150 n'existe pas

### 2.10 - Requête n° 10 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/ingredients/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk"
```

    Description : renvoie une erreur 400 - Requête invalide

## 3 - Endpoints pizzas : /pizzas/*

### 3.1 - Requête n° 1 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas"
```

    Description : renvoie la liste de toutes les pizzas et de leurs ingrédients au format JSON

### 3.2 - Requête n° 2 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/101"
```

    Description : renvoie la pizza n°101 ainsi que ses ingrédients au format JSON

### 3.3 - Requête n° 3 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/101/prixfinal"
```

    Description : renvoie le prix final de la pizza n°101 (float arrondi à deux chiffres après la virgule). Le prix est calculé à partir du prix de base de la pizza + le prix de ses ingrédients.

### 3.4 - Requête n° 4 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/101/abcde"
```

    Description : renvoie une erreur 400 - Requête invalide

### 3.5 - Requête n° 5 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "{
            \"pno\": 110,
            \"name\": \"montagnard\",
            \"base\": \"classique\",
            \"basePrice\": 10.0,
            \"ingredients\": [
                {
                    \"ino\": 1
                },
                {
                    \"ino\": 4
                },
                {
                    \"ino\": 6
                },
                {
                    \"ino\": 8
                }
            ]
        }"
```

    Description : renvoie la pizza que l'on vient d'ajouter ainsi que ses ingrédients au format JSON. Ajoute la pizza à la base de données.

### 3.6 - Requête n° 6 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/" \
    --header "Content-Type: text/plain" \
    --data "{
            \"pno\": 111,
            \"name\": \"champignard\",
            \"base\": \"fine\",
            \"basePrice\": 10.0,
            \"ingredients\": [
                {
                    \"ino\": 2
                },
                {
                    \"ino\": 5
                },
                {
                    \"ino\": 6
                },
                {
                    \"ino\": 7
                },
                {
                    \"ino\": 10
                }
            ]
        }"
```

    Description : renvoie une erreur 401 - Non autorisé car il manque le token JWT

### 3.7.1 - Requête n° 7.1 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "{
            \"pno\": 112,
            \"name\": \"montagnard\",
            \"base\": \"classique\",
            \"basePrice\": 10.0,
            \"ingredients\": [
                {
                    \"ino\": 1000
                },
                {
                    \"ino\": 4
                },
                {
                    \"ino\": 6
                },
                {
                    \"ino\": 8
                }
            ]
        }"
```

    Description : renvoie une erreur 404 - Non trouvé car l'ingrédient n°1000 n'existe pas. La pizza n°112 ne sera pas créee

### 3.7.2 - Requête n° 7.2 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "{
            \"pno\": 110,
            \"name\": \"montagnard\",
            \"base\": \"classique\",
            \"basePrice\": 10.0,
            \"ingredients\": [
                {
                    \"ino\": 2
                },
                {
                    \"ino\": 4
                },
                {
                    \"ino\": 6
                },
                {
                    \"ino\": 8
                }
            ]
        }"
```

    Description : renvoie une erreur 409 - Conflit car la pizza n°110 existe déjà

### 3.7.3 - Requête n° 7.3 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "{
            \"pno\": 110,
            \"name\": \"montagnard\",
            \"base\": \"classique\",
            \"basePrice\": 10.0,
            \"ingredients\": [
                {
                    \"ino\": 1000
                },
                {
                    \"ino\": 4
                },
                {
                    \"ino\": 6
                },
                {
                    \"ino\": 8
                }
            ]
        }"
```

    Description : renvoie une erreur 409 - Conflit car la pizza n°110 existe déjà et l'ingrédient n°1000 n'existe pas mais nous mettons la priorité sur l'erreur 409. La requête est annulée, rien ne change au niveau de la base de données.

### 3.8 - Requête n° 8 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/110/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "11"
```

    Description : renvoie l'ingrédient n°11 au format JSON. Et ajoute l'ingrédient n°11 à la pizza n°110 dans la base de données.

### 3.9 - Requête n° 9 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/110/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "42"
```

    Description : renvoie une erreur 404 - Non trouvé car l'ingrédient n°42 n'existe pas

### 3.10 - Requête n° 10 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/110/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "11"
```

    Description : renvoie une erreur 409 - Conflit car l'ingrédient n°11 est déjà dans la pizza n°110

### 3.11 - Requête n° 11 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/pizzas/110/" \
    --header "Content-Type: text/plain" \
    --data "2"
```

    Description : renvoie une erreur 401 - Non autorisé car il manque le token JWT

### 3.12 - Requête n° 12 - PATCH

```shell
    curl --location --request PATCH "http://localhost:8080/pizzeria/pizzas/110/prixBase" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "12"
```

    Description : renvoie une chaine de caractère : "montagnard(110) new base price : 12.0€.
    Final price is 15.9€". Et change le prix de base de la pizza n°110 (et par conséquent change également son prix final) dans la base de donnée.

### 3.13 - Requête n° 13 - PATCH

```shell
    curl --location --request PATCH "http://localhost:8080/pizzeria/pizzas/110/prixBase" \
    --header "Content-Type: text/plain" \
    --data "12"
```

    Description : renvoie une erreur 401 - Non autorisé car il manque le token JWT

### 3.14 - Requête n° 14 - PATCH

```shell
    curl --location --request PATCH "http://localhost:8080/pizzeria/pizzas/110/name" \
        --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
        --header "Content-Type: text/plain" \
        --data "savoyarde"
```

    Description : renvoie la chaine de caractère "Pizza 110 new name : savoyarde". Et change le nom de la pizza n°110 en "savoyarde" dans la base de donnée.

### 3.15 - Requête n° 15 - PATCH

```shell
    curl --location --request PATCH "http://localhost:8080/pizzeria/pizzas/110/abcde" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "savoyarde"
```

    Description : renvoie une erreur 400 - Requête invalide

### 3.16 - Requête n° 16 - PATCH

```shell
    curl --location --request PATCH "http://localhost:8080/pizzeria/pizzas/110/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "12"
```

    Description : renvoie une erreur 400 - Requête invalide

### 3.17 - Requête n° 17 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/pizzas/110/1"
```

    Description : renvoie une erreur 401 - Non autorisé car il manque le token JWT

### 3.18 - Requête n° 18 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/pizzas/110/2" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk"
```

    Description : renvoie une erreur 404 - Non trouvé car la pizza n°110 ne contient pas l'ingrédient n°2. Balise message de l'erreur 404 : "savoyarde (110) does not contain ingredient 2"

### 3.19 - Requête n° 19 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/pizzas/110/50" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk"
```

    Description : renvoie une erreur 404 - Non trouvé car l'ingrédient n°50 n'existe pas. Balise message de l'erreur 404 : "Ingredient 50 does not exist"

### 3.20 - Requête n° 20 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/pizzas/110/8" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk"
```

    Description : renvoie une chaine de caractère : "You have removed the ingredient fromage(8) from pizza montagnard(110)". Et supprime l'ingrédient n°8 de la pizza n°110 dans la base de données.

### 3.21 - Requête n° 21 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/pizzas/110"
```

    Description : renvoie une erreur 401 - Non autorisé car il manque le token JWT

### 3.22 - Requête n° 22 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/pizzas/200/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk"
```

    Description : renvoie une erreur 404 - Non trouvé car la pizza n°200 n'existe pas. Balise message de l'erreur 404 : " You can't remove pizza n°200 because it does not exist "

### 3.23 - Requête n° 23 - DELETE

```shell
    curl --location --request DELETE "http://localhost:8080/pizzeria/pizzas/110/" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk"
```

    Description : renvoie une chaine de caractère : "You have removed the pizza montagnard(110)". Et supprime la pizza n°110 de la base de données

## 4 - Endpoints commandes : /commandes/*

### 4.1 - Requête n° 1 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/commandes"
```

    Description : renvoie la liste des commandes avec leurs pizzas et les ingrédients des pizzas au format JSON

### 4.2 - Requête n° 2 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/commandes/1"
```

    Description : renvoie la commande n°1 avec ses pizzas et leurs ingrédients au format JSON

### 4.3 - Requête n° 3 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/commandes/1/prixfinal"
```

    Description : renvoie le prix final de la commande n°1. Le prix final est un float (2 chiffres après la virgule). Il est calculé en additionnant le prix final de chaque pizza présente dans la commande.


### 4.4 - Requête n° 4 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/commandes/10/"
```

    Description : renvoie une erreur 404 - Non trouvé car la pizza n°10 n'existe pas.


### 4.5 - Requête n° 5 - GET

```shell
    curl --location "http://localhost:8080/pizzeria/commandes/1/abcd"
```

    Description : renvoie une erreur 400 - Requête invalide

### 4.6 - Requête n° 6 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/commandes" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "{
        \"cno\": 10,
        \"uno\": 1,
        \"commandeDate\": \"2023-10-10\",
        \"pizzas\": [
            {
                \"pno\": 100
            },
            {
                \"pno\": 101  
            },
            {
                \"pno\": 101
            }
        ]
    }"
```

    Description : renvoie la commande n°10 avec ses pizzas et leurs ingrédients au format JSON. Et ajoute la commande n°10 dans la base de données.

### 4.7 - Requête n° 7 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/commandes" \
    --header "Content-Type: text/plain" \
    --data "{
        \"cno\": 10,
        \"uno\": 1,
        \"commandeDate\": \"2023-10-10\",
        \"pizzas\": [
            {
                \"pno\": 100
            },
            {
                \"pno\": 101  
            },
            {
                \"pno\": 101
            }
        ]
    }"
```

    Description : renvoie une erreur 401 - Non autorisé car il manque le token JWT


### 4.8 - Requête n° 8 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/commandes/abc" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "{
        \"cno\": 10,
        \"uno\": 1,
        \"commandeDate\": \"2023-10-10\",
        \"pizzas\": [
            {
                \"pno\": 100
            },
            {
                \"pno\": 101  
            },
            {
                \"pno\": 101
            }
        ]
    }"
```

    Description : renvoie une erreur 400 - Requête invalide

### 4.9.1 - Requête n° 9.1 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/commandes" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "{
        \"cno\": 20,
        \"uno\": 1,
        \"commandeDate\": \"2023-10-10\",
        \"pizzas\": [
            {
                \"pno\": 999
            },
            {
                \"pno\": 101  
            },
            {
                \"pno\": 101
            }
        ]
    }"
```

    Description : renvoie une erreur 404 - Non trouvé car la pizza n°999 n'existe pas dans la base de données. La requête est annulée et rien n'est ajouté à la base de données

### 4.9.2 - Requête n° 9.2 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/commandes" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "{
        \"cno\": 1,
        \"uno\": 1,
        \"commandeDate\": \"2023-10-10\",
        \"pizzas\": [
            {
                \"pno\": 100
            },
            {
                \"pno\": 101  
            },
            {
                \"pno\": 101
            }
        ]
    }"
```

    Description : renvoie une erreur 409 - Conflit car la commande n°1 existe déjà

### 4.9.3 - Requête n° 9.3 - POST

```shell
    curl --location "http://localhost:8080/pizzeria/commandes" \
    --header "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJiN2RmY2JmZGExYmM0MDBiOGMzNzc4MzEyMzZiMGM5NCIsImlhdCI6MTY3Nzg0MDAyMCwic3ViIjoiQXV0aGVudGlmaWNhdGlvbiBwb3VyIHBpenplcmlhIiwiaXNzIjoiUmVuYW4uTmF0aGFuIiwiZXhwIjoxNjc4ODc5MjQ5fQ.604mVviPvcy1TniXcNbBubE2j3fLxYus0DPeDVde6rk" \
    --header "Content-Type: text/plain" \
    --data "{
        \"cno\": 1,
        \"uno\": 1,
        \"commandeDate\": \"2023-10-10\",
        \"pizzas\": [
            {
                \"pno\": 999
            },
            {
                \"pno\": 101  
            },
            {
                \"pno\": 101
            }
        ]
    }"
```

    Description : renvoie une erreur 409 - Conflit car la pizza n°1 existe déjà et la pizza n°999 n'existe pas dans la base de données. Priorité à l'erreur 409.

!!! info Informations
    Vous pouvez aussi retrouver la collection ``Postman`` des endpoints au format JSON via le fichier ``Pizzeria.postman_collection.json``