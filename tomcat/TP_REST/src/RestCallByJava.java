import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class RestCallByJava{
    public static void main(String[] args) throws IOException {
        URL url = new URL("https://atom.iut-info.univ-lille.fr/planetary/apod?api_key=DEMO_KEY");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setRequestMethod("GET");

        // envoi des données pour la requête (PAS BESOIN CAR GET)

        // récupération des données en retour
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null)
        System.out.println(inputLine);
        in.close();
        con.disconnect();
    }
}