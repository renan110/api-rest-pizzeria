import java.sql.Date;

public class Nasa {
    private String copyright;
    private Date date;
    private String explanation;
    private String hdurl;
    private String media_type;
    private String service_version;
    private String title;
    private String url;


    public Nasa() {
    }

    public Nasa(String copyright, Date date, String explanation, String hdurl, String media_type, String service_version, String title, String url) {
        this.copyright = copyright;
        this.date = date;
        this.explanation = explanation;
        this.hdurl = hdurl;
        this.media_type = media_type;
        this.service_version = service_version;
        this.title = title;
        this.url = url;
    }

    public String getCopyright() {
        return this.copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getExplanation() {
        return this.explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public String getHdurl() {
        return this.hdurl;
    }

    public void setHdurl(String hdurl) {
        this.hdurl = hdurl;
    }

    public String getMedia_type() {
        return this.media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getService_version() {
        return this.service_version;
    }

    public void setService_version(String service_version) {
        this.service_version = service_version;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Nasa copyright(String copyright) {
        setCopyright(copyright);
        return this;
    }

    public Nasa date(Date date) {
        setDate(date);
        return this;
    }

    public Nasa explanation(String explanation) {
        setExplanation(explanation);
        return this;
    }

    public Nasa hdurl(String hdurl) {
        setHdurl(hdurl);
        return this;
    }

    public Nasa media_type(String media_type) {
        setMedia_type(media_type);
        return this;
    }

    public Nasa service_version(String service_version) {
        setService_version(service_version);
        return this;
    }

    public Nasa title(String title) {
        setTitle(title);
        return this;
    }

    public Nasa url(String url) {
        setUrl(url);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " copyright='" + getCopyright() + "'" +
            ", date='" + getDate() + "'" +
            ", explanation='" + getExplanation() + "'" +
            ", hdurl='" + getHdurl() + "'" +
            ", media_type='" + getMedia_type() + "'" +
            ", service_version='" + getService_version() + "'" +
            ", title='" + getTitle() + "'" +
            ", url='" + getUrl() + "'" +
            "}";
    }
    

}
