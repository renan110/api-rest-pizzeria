##  Interroger un service REST extérieur

Avec CURL:

    curl -X GET api.zippopotam.us/fr/06800
    curl -X GET api.zippopotam.us/fr/86000
    curl -X GET api.zippopotam.us/tr/35000

Avec Httpie: (Sur windows il faut installer ...)

    https api.zippopotam.us/fr/06800
    https api.zippopotam.us/fr/86000
    https api.zippopotam.us/tr/35000

- http://universities.hipolabs.com/ 

Avec CURL:

    curl -X GET universities.hipolabs.com/search?name=Lil

    curl -X GET universities.hipolabs.com/search?country=belgium

    curl -X GET 'universities.hipolabs.com/search?name=Middle&united States'

Avec Httpie:

    http universities.hipolabs.com/search?name=Lil

    http universities.hipolabs.com/search?country=belgium

    http 'universities.hipolabs.com/search?name=Middle&united States'


## Interroger un service REST local

!!! question Notez les différents champs caractérisant une ressource
    ```xml
    <Etudiant>
        <id>55</id>
        <prenom>Julien</prenom>
        <nom>DELCOURT</nom>
        <login>julien.delcourt.etu</login>
        <groupe>I</groupe>
        <dnaiss/>
        <ville/>
    </Etudiant>
    ```

Avec control + F , je recherche Renan

```xml
<item>
    <id>75</id>
    <prenom>Renan</prenom>
    <nom>DECLERCQ</nom>
    <login>renan.declercq.etu</login>
    <groupe>J</groupe>
    <dnaiss/>
    <ville/>
</item>
```

!!! question Ajouter Paul DUCHEMIN dans le groupe K
    ```shell
    curl -i -H "Content-Type:application/json" -d "{"nom":"DUCHEMIN", "prenom":"Paul", "groupe":"K"}" -X POST "http://localhost:8080/tp331/etudiants"
    ```

    !!! warning
        Problème à cause de l'id je crois mais l'idée est là
    
    ```shell
    curl -X 'POST' \
    'http://localhost:8080/tp331/etudiants' \
    -H 'accept: application/json' \
    -H 'Content-Type: application/json' \
    -d '{
    "id": 0,
    "prenom": "Paul",
    "nom": "DUCHEMIN",
    "login": "",
    "groupe": "K",
    "dnaiss": "2023-02-02T09:06:28.389Z",
    "ville": ""
    }'
    ```

!!! info Possible avec httpie : Exemple
    ```shell
    http POST https://jsonplaceholder.typicode.com/users
    email="jb@is.gb" name="James Bond" phone="+(33) 007"
    ```

Ajouter un étudiant: Paul DUCHEMIN dans le groupe K

    curl -i -H 'Content-Type: application/json' -d '{"nom":"DUCHEMIN","prenom":"Paul","groupe":"K"}' -X POST localhost:8080/tp331/etudiants

    http POST localhost:8080/tp331/etudiants nom="DUCHEMIN" prenom="Paul" groupe="K"

Modifier une partie de mes infos : PATCH

    curl -i -H 'Content-Type: application/json' -d '{"dnaiss":"2002-04-09","ville":"Lille"}' -X PATCH localhost:8080/tp331/etudiants/24

    http PATCH localhost:8080/tp331/etudiants/24 dnaiss="2002-04-09" ville="Lille" groupe="H"

Modifier une partie de mes infos : PUT

    curl -i -H 'Content-Type: application/json' -d '{"dnaiss":"2002-04-09","ville":"VDA"}' -X PUT localhost:8080/tp331/etudiants/24

    http PUT localhost:8080/tp331/etudiants/24 dnaiss="2002-04-09" ville="Lille" groupe="H"

Le PUT efface tout et renseigne les éléments mis contrairement au PATCH qui modifie juste les éléments a modifier.

    http PATCH localhost:8080/tp331/etudiants/24 prenom="nathan" nom="ACCART" login="nathan.accart.etu" dnaiss="2002-04-09" ville="Lille" groupe="H"

### Content negociation

```shell
curl -X 'POST' \
  'http://localhost:8080/tp331/etudiants' \
  -H 'accept: application/xml' \
  -H 'Content-Type: application/json' \
  -d '{
  "id": 0,
  "prenom": "string",
  "nom": "string",
  "login": "string",
  "groupe": "string",
  "dnaiss": "2023-02-02T10:20:24.579Z",
  "ville": "string"
}'
```

!!! info Informations Changer ces deux lignes pour choisir xml / json
    -H 'accept: application/xml' \
    -H 'Content-Type: application/json' \

## Interfaces graphiques REST

http://localhost:8080/tp331/swagger-ui/index.html#/

